package com.vapasi.thoughtworks.rectangle;

public class Rectangle {
    protected int length;
    protected int breadth;

    public Rectangle(int length, int breadth) {
        if (length == 0 || breadth == 0)
            throw new IllegalArgumentException("Illegal Arguments for a rectangle");
        this.length = length;
        this.breadth = breadth;

    }

    public static Rectangle createSquare(int length) {
        return new Rectangle(length,length);
    }

    public int area() {
        return length * breadth;
    }

    public int perimeter() {
        return 2 * (length + breadth);
    }

}
