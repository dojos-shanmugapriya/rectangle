package com.vapasi.thoughtworks.rectangle.test;

import com.vapasi.thoughtworks.rectangle.Rectangle;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class RectangleTest {

    @ParameterizedTest
    @CsvSource({"3,5,15","5,5,25"})
    void shouldCalculateRectangleArea(int length,int breadth,int result) {
        assertEquals(result, new Rectangle(length,breadth).area());
    }

    @ParameterizedTest
    @CsvSource({"3,5,16","5,5,20"})
    void shouldCalculateRectanglePerimeter(int length,int breadth,int result)
    {
        assertEquals(result, new Rectangle(length,breadth).perimeter());
    }

    @ParameterizedTest
    @CsvSource({"3,9","5,25"})
    void shouldCalculateSquareArea(int length,int result) {
        assertEquals(result, Rectangle.createSquare(length).area());
    }

    @ParameterizedTest
    @CsvSource({"6,24","4,16"})
    void shouldCalculateSquarePerimeter(int length,int result) {
        assertEquals(result, Rectangle.createSquare(length).perimeter());
    }

    @Test
    void shouldThrowExceptionWhenEitherLengthOrBreadthIsZero() {
        assertThrows(IllegalArgumentException.class,() -> {new Rectangle(0,2);});
    }


}
